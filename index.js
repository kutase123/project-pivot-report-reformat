// ==UserScript==
// @name         Project Pivot Report Reformat
// @namespace    http://tampermonkey.net/
// @version      1.1
// @description  Project Pivot Report Reformat plugin for JIRA
// @author       Kutase
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js
// @require      https://raw.githubusercontent.com/lightswitch05/table-to-json/master/lib/jquery.tabletojson.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js
// @match http://j.jetstyle.ru/secure/Dashboard.jspa
// ==/UserScript==

'use strict'

var htmlTemplate = `
<table class="grid">
  <tbody>
    <tr class="rowHeader" style="font-weight: bold;">
      <td>
        <span>Задача</span>
      </td>
      <td style="width: 35%;">
        <span>Исполнитель</span>
      </td>
      <!-- <td>
        <span>Информация</span>
      </td> -->
      <td style="max-width: 10em;">
        <span>Общее количество часов</span>
      </td>
    </tr>

    <!-- ko foreach: tasks -->
      <tr class="rowNormal" style="cursor: pointer;" data-bind="click: opened() ? closeList : openList">
        <td>
          <span style="font-weight: bold;">
            <a target="_blank" data-bind="attr: {href: location.origin+'/browse/'+name}, text: name"></a>
          </span>
        </td>
        <td>
          <div data-bind="foreach: users">
            <div>
              <span>
                <a target="_blank" data-bind="attr: {href: href}, text: name"></a>
              </span>
              <span> </span>
              <span style="font-weight: bold;" data-bind="text: hours"></span>
            </div>
          </div>
        </td>
        <td style="max-width: 10em;">
          <span style="font-weight: bold;" data-bind="text: total"></span>
        </td>
      </tr>
      <!-- ko if: opened -->
        <!-- ko foreach: $data.tasks -->
          <tr class="rowAlternate">
            <td>
              <div>
                <a style="font-size: 0.9em;" target="_blank" data-bind="attr: {href: location.origin+'/browse/'+name}">
                  <strong data-bind="text: name.split('-')[1]+' '"></strong>
                  <span data-bind="text: data.taskDesc"></span>
                </a>
              </div>
            </td>
            <td>
              <div data-bind="foreach: users">
                <div>
                  <span>
                    <a target="_blank" data-bind="attr: {href: href}, text: name"></a>
                  </span>
                  <span> </span>
                  <span style="font-weight: bold;" data-bind="text: hours"></span>
                </div>
              </div>
            </td>
            <!-- ko with: data -->
              <td style="max-width: 10em;">
                <span style="font-weight: bold;" data-bind="text: total"></span>
              </td>
            <!-- /ko -->
          </tr>
        <!-- /ko -->
      <!-- /ko -->
    <!-- /ko -->
  </tbody>
</table>`;

function formatJSON (json, usersHrefs) {
  var result = {};
  var cleanedJSON = [];

  json.forEach((el, i) => {
    let newObj = {};

    for (let prop in el) {
      if (el[prop] != '0h' && ['task', 'priority'].indexOf(prop) == -1) {
        newObj[prop] = el[prop];
      }
    }

    cleanedJSON.push(newObj);
  })

  cleanedJSON.forEach((el, i) => {
    var groupTaskName = el['taskName'].split('-')[0];
    if (!result.hasOwnProperty(groupTaskName)) {
      result[groupTaskName] = {};
    }

    var taskName = el['taskName'];

    result[groupTaskName][taskName] = el;
  })

  var arrayResult = [];

  for (let prop in result) {
    var task = result[prop]
    var total = 0;

    let parentUsers = [];

    for (let prop in task) {
      let childTask = task[prop];

      total += parseFloat(task[prop]['Tot'].replace(',', '.').replace(/\s/g,''));

      let users = [];

      for (let childProp in childTask) {
        if (['Tot', 'taskDesc', 'taskName'].indexOf(childProp) == -1) {
          let user = {
            name: childProp,
            hours: Math.floor(parseFloat(childTask[childProp].replace(',', '.').replace(/\s/g,'')) * 10) / 10,
            href: usersHrefs[childProp]
          }

          users.push(user);
          parentUsers.push(user);
        }
      }

      childTask.users = users;

      let minParentUsers = [];

      parentUsers.forEach((el, i) => {
        var currentUser = minParentUsers.find(user => user.name == el.name);

        if (currentUser) {
          currentUser.hours += el.hours;
        } else {
          minParentUsers.push({
            name: el.name,
            hours: el.hours,
            href: usersHrefs[el.name]
          });
        }
      })

      minParentUsers.forEach(el => el.hours = Math.floor(el.hours * 10) / 10)

      task.users = minParentUsers;
    }

    task.total = Math.floor(total * 10) / 10;

    let tasks = [];

    for (let prop in task) {
      if (['users', 'total'].indexOf(prop) == -1) {
        let childTask = task[prop];

        tasks.push({
          name: prop,
          users: childTask.users,
          data: {
            total: Math.floor(parseFloat(childTask.Tot.replace(',', '.').replace(/\s/g,'')) * 10) / 10,
            taskDesc: childTask.taskDesc
          }
        })
      }
    }

    arrayResult.push({
      name: prop,
      tasks: tasks,
      users: result[prop].users,
      total: Math.floor(result[prop].total * 10) / 10
    })
  }

  return arrayResult;
}

function renderJSON (tasks, tableEl) {

  tableEl.empty();
  tableEl.html(htmlTemplate);

  tasks.forEach(el => {
    el.opened = ko.observable(false);
    el.openList = () => el.opened(true);
    el.closeList = () => el.opened(false);
  })

  let VM = {
    tasks: tasks
  }

  ko.applyBindings(VM, tableEl[0]);
}

function createNewTable (table, iframe) {
  var headings = ['task', 'taskName', 'taskDesc', 'priority'];

  var usersHrefs = {};

  var tableHeadingsRow = table.find('.rowHeader').find('td').toArray();
  tableHeadingsRow.forEach((el, i) => {
    if (i > 0) {
      if (i != tableHeadingsRow.length - 1) {
        var userName = $(el).find('a').text();
        headings.push(userName);
        usersHrefs[userName] = $(el).find('a').attr('href');
      } else {
        headings.push($(el).text());
      }
    }
  })

  var rows = table.find('tr');

  $(rows[0]).remove();
  $(rows[rows.length-1]).remove();

  var json = table.tableToJSON({
    headings: headings
  });

  var newJSON = formatJSON(json, usersHrefs);

  renderJSON(newJSON, table);

  let divContainer = iframe.find('.results-wrap>div');

  divContainer.css({
    'overflow': 'auto',
    'height': (table.height() + 10) + 'px'
  });

  iframe.css({
    'height': (table.height() + 10) + 'px'
  })
}

$(() => {
  let timeoutTicks = 30;
  let timeoutCounter = 0;

  let interval = setInterval(() => {
    if (timeoutCounter < timeoutTicks) {
      var list = $('.gadget').toArray()

      var pivotReporter = list.filter(el => {
        return $(el).find('h3').text() == "Project Pivot Report";
      })

      if (pivotReporter.length != 0) {
        let table = $(pivotReporter).find('iframe').contents().find('table');

        if (table.length == 0) {
          timeoutCounter++;
        } else if (table.length == 1) {
          clearInterval(interval);
          createNewTable(table, $(pivotReporter).find('iframe').contents());
        }
      } else {
        timeoutCounter++;
      }
    } else {
      clearInterval(interval);

      console.error('Gadget is not found in this dashboard!');
    }

  }, 1000)
})
