1. Установить [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=ru)
2. Склонировать репозиторий
3. Создать новый скрипт в Tampermonkey
4. Скопировать весь код из index.js в скрипт Tampermonkey и сохранить
5. Установить гаджет Project Pivot Reporter в JIRA
6. Перезагрузить страницу